<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width; initial-scale=1">
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>
    <style>
body{
  background-color:#F2F5A9;
}
.main-form{
    width:60%;
    display: flex;
    background-color:#B5DFC8;
  justify-content:center;
  border-radius:20px; 
  margin-left:20%;
}
form{
    text-align: center;
}
.errors {
  color: red;
}

.error {
  border: 2px solid red;
}

</style>
    <div class="cover-block"> 
<form class="transparent" action="" method="POST">
<div class="form">
<h3>Application</h3>
<label>
       <p>Put your name: </p> 
   <input name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>" placeholder="Name put">
   <?php if (!empty($messages['fio'])) {print($messages['fio']);} ?>
   </label>
  <label>
       <p>Put e-mail: </p>
   <input name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" type="email" placeholder="test@example.com"> 
   <?php if (!empty($messages['email'])) {print($messages['email']);} ?>
      </label>
  <label>
      <p>Put your data of born: </p>
  <input name="date" <?php if ($errors['date']) {print 'class="error"';} ?> value="<?php print $values['date']; ?>" type="date" min="1920-01-01" max="2010-01-01">
  <?php if (!empty($messages['date'])) {print($messages['date']);} ?>
      </label>
     <label>
      <p>Put your gender:<br>
      <label class="radio"><input name="sex" <?php if ($errors['sex']) {print 'class="error"';} ?> checked="<?php if($values['sex'] == 'woman'){print 'checked';} ?>" type="radio" value="woman"> Genshina</label> 
      <label class="radio"><input name="sex" <?php if ($errors['sex']) {print 'class="error"';} ?> checked="<?php if($values['sex'] == 'man'){print 'checked';} ?>" type="radio" value="man"> Mujic</label></br></p>
      <?php if (!empty($messages['sex'])) {print($messages['sex']);} ?>
 </label> 
 <label>
      <p>Put kol-vo ends: <br>
	  <label class="radio"><input name="edu" type="radio" value="1" checked="<?php if($values['edu'] == '1') {print 'checked';} ?>"> 1</label>
      <label class="radio"><input name="edu" type="radio" value="2" checked="<?php if($values['edu'] == '2') {print 'checked';} ?>"> 2</label>
      <label class="radio"><input name="edu" type="radio" value="3" checked="<?php if($values['edu'] == '3') {print 'checked';} ?>"> 3</label>
      <label class="radio"><input name="edu" type="radio" value="4" checked="<?php if($values['edu'] == '4') {print 'checked';} ?>"> 4</label>
      <label class="radio"><input name="edu" type="radio" value="5" checked="<?php if($values['edu'] == '5') {print 'checked';} ?>"> 5</label><br></p>
      <?php if (!empty($messages['edu'])) {print($messages['edu']);} ?>
 </label>
 <label>
      <p>Put your sverhsposobnosti:<</p>
  <select name="sverh" multiple="multiple">
<option value="Speed">Speed</option>
<option value="Power">Power</option>
<option value="Three">3 po diffuram</option>
<option value="Fly">Fly</option>
<option value="Nichego">Nichego</option>
  </select>
</label>
<?php if (!empty($messages['course'])) {print($messages['course']);} ?>
   <label>
      <p>Комментарий:</p>
		<textarea name="comment" <?php if ($errors['comment']) {print 'class="error"';} ?> rows="3" cols="40"><?php print $values['comment']; ?></textarea>
          </label>
          <?php if (!empty($messages['comment'])) {print($messages['comment']);} ?>
  <label>
      <p>Подпись</p>
		<input name="check" <?php if ($errors['check']) {print 'class="error"';} ?> checked="<?php if($values['check']) print 'checked'; ?>" type="checkbox" id="check"> Поставьте галочку.
        </label>
        <?php if (!empty($messages['check'])) {print($messages['check']);} ?>
  
  <p> <input type="submit" value="SEND" /></p>
  </div>
</form>
</div>
</body>
</html>
